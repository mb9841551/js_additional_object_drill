const users = require("./problemStatement.js"); // Importing problemStatement.js file

function getUserWithMastersDegree(obj){
    let mastersArray = [];          //empty array

    Object.keys(obj).forEach((user) => {
        if(obj[user].qualification === "Masters"){
            mastersArray.push(user);
        }
    })
    return mastersArray;
}

console.log(getUserWithMastersDegree(users));    