const obj = require("./problemStatement.js");

function groupUsersByProgrammingLanguage(obj) {
  const groupedUsers = {};

  Object.keys(obj).forEach((user) => {
    const programmingLanguage = extractProgrammingLanguage(obj[user].desgination);
    if (groupedUsers.hasOwnProperty(programmingLanguage)) {
      groupedUsers[programmingLanguage].push(user);
    } else {
      groupedUsers[programmingLanguage] = [user];
    }
  });
  return groupedUsers;
}

// to extract programming language from designation
function extractProgrammingLanguage(desgination) {
  const programmingLanguages = ["Golang", "Javascript", "Python"];

  // Check if designation is defined before attempting to find the programming language
  const foundLanguage = desgination && programmingLanguages.find((language) =>
    desgination.includes(language)
  );

  return foundLanguage || "Unknown";
}

const result = groupUsersByProgrammingLanguage(obj);
console.log(result);
