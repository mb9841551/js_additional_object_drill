const users = require("./problemStatement.js"); // Importing problemStatement.js file

function livingInGermany(obj) {
  let germanyUser = []; //empty array

  Object.keys(obj).forEach((user) => {
    if(obj[user].nationality === "Germany"){
      germanyUser.push(user);
    }
  })
  return germanyUser;
}

console.log(livingInGermany(users));
