const object = require("./problemStatement");       // Importing problemStatement.js file

const sortedByDesignation = Object.keys(object).sort((userName1, userName2) => {            //to get keys of the object
  const user1 = object[userName1]; 
  const user2 = object[userName2]; 
  const designationLevel = {     //assigning seniority level
    "Senior Golang Developer": 3,
    "Senior Javascript Developer": 3,
    "Python Developer": 2,
    "Intern - Javascript": 1,
    "Intern - Golang": 1,
  };
  const SeniorLevel1 = designationLevel[user1.desgination] || 0; //to give ranks
  const SeniorLevel2 = designationLevel[user2.desgination] || 0; //to give ranks

  if (SeniorLevel1 !== SeniorLevel2) {
    
    return SeniorLevel2 - SeniorLevel1; //if -ve value is returned second argument added second and than first argument and vice-versa
  }
  return user2.age - user1.age; //compare with age if ranks are same
});
console.log(sortedByDesignation); 
